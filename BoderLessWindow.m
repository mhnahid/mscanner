//
//  BoderLessWindow.m
//  MalwareScannerSt
//
//  Created by marvellous on 6/22/16.
//  Copyright © 2016 Cor2Tect. All rights reserved.
//

#import "BoderLessWindow.h"

@implementation BoderLessWindow
- (instancetype)initWithContentRect:(NSRect)contentRect styleMask:(NSUInteger)aStyle backing:(NSBackingStoreType)bufferingType defer:(BOOL)flag
{
    self = [super initWithContentRect:contentRect styleMask:aStyle|NSFullSizeContentViewWindowMask backing:bufferingType defer:flag];
    
    self.titleVisibility = NSWindowTitleHidden;
    self.titlebarAppearsTransparent = YES;
    self.movableByWindowBackground = YES;
    
    return self;
}

- (void)setContentView:(__kindof NSView *)contentView
{
    [super setContentView:contentView];
    
    [self _so_hideTrafficLights];
}

- (void)_so_hideTrafficLights
{
    for (id subview in self.contentView.superview.subviews) {
        if ([subview isKindOfClass:NSClassFromString(@"NSTitlebarContainerView")]) {
            NSView *titlebarView = [subview subviews][0];
            for (id button in titlebarView.subviews) {
                if ([button isKindOfClass:[NSButton class]]) {
                    [button setHidden:YES];
                }
            }
        }
    }
}

@end
